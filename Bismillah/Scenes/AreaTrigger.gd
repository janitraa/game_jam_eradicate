extends Area2D

export (String) var sceneName = "WinScreen"

func _on_AreaTrigger_body_entered(body):
    if body.get_name() == "KinematicBody2D":
        get_tree().change_scene(str("res://Scenes/" + sceneName + ".tscn"))
