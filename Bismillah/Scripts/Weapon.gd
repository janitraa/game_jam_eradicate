extends Area2D

export (int) var speed = 100

var velocity = Vector2()
var direction = 1

func _ready():
	pass # Replace with function body.

func set_weapon_direction(dir):
	direction = dir
	if dir == -1:
		$AnimatedSprite.flip_h = true
		
func _physics_process(delta):
	velocity.x = speed * delta * direction
	translate(velocity)
	$AnimatedSprite.play("shoot")

func _on_VisibilityNotifier2D_screen_exited():
	queue_free() # Replace with function body.

func _on_Weapon_body_entered(body):
	if "Trap" in body.name:
		body.dead()
	queue_free()
