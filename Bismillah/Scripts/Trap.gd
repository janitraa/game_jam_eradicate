extends KinematicBody2D

export (int) var GRAVITY = 10
export (int) var speed = 30

const UP = Vector2(0, -1)

var velocity = Vector2()
var direction = 1
var is_dead = false

func _ready():
	pass # Replace with function body.

func dead():
	is_dead = true
	velocity = Vector2(0,0)
	$AnimatedSprite.play("dead") 
	$CollisionShape2D.disabled = true
	$Timer.start()
	
func _physics_process(delta):
	if is_dead == false:
		velocity.x = speed * direction
		
		if direction == 1:
			$AnimatedSprite.flip_h = true
		else:
			$AnimatedSprite.flip_h = false
			
		$AnimatedSprite.play("fly")
		velocity.y += GRAVITY
		
		velocity = move_and_slide(velocity, UP)
	
		if is_on_wall():
			direction *= -1
			$RayCast2D.position.x *= -1
				
		if $RayCast2D.is_colliding() == false:
			direction *= -1
			$RayCast2D.position.x *= -1


func _on_Timer_timeout():
	queue_free() # Replace with function body.
