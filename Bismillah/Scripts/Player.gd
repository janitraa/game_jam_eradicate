extends KinematicBody2D

export (int) var speed = 40
export (int) var GRAVITY = 10
export (int) var jump_speed = -280

const WEAPON = preload("res://Scenes/Weapon.tscn")
const UP = Vector2(0,-1)

var velocity = Vector2(speed,0)
var is_dead = false

func _physics_process(delta):
	if is_dead == false:
		velocity.x += 0.2
	
		if Input.is_action_just_pressed('ui_right'):
			velocity.x = speed
			$AnimatedSprite.play("walk")
			$AnimatedSprite.flip_h = false
			if sign($Position2D.position.x) == -1:
				$Position2D.position.x *= -1
		elif Input.is_action_just_pressed('ui_left'):
			velocity.x = -speed
			$AnimatedSprite.play("walk")
			$AnimatedSprite.flip_h = true
			if sign($Position2D.position.x) == 1:
				$Position2D.position.x *= -1
		
		if is_on_floor() and Input.is_action_just_pressed('ui_up'):
			velocity.y = jump_speed
			$AnimatedSprite.play("jump")
			
		if Input.is_action_just_pressed("ui_focus_next"):
			var weapon = WEAPON.instance()
			if sign ($Position2D.position.x) == 1:
				weapon.set_weapon_direction(1)
			else:
				weapon.set_weapon_direction(-1)
					
			get_parent().add_child(weapon)
			weapon.position = $Position2D.global_position
			
		velocity.y += GRAVITY
		velocity = move_and_slide(velocity, UP)
		
		if get_slide_count() > 0:
			for i in range(get_slide_count()):
				if "Trap" in get_slide_collision(i).collider.name:
					dead()
		
func dead():
	is_dead = true
	velocity = Vector2(0,0)
	$AnimatedSprite.play("dead")
	$CollisionShape2D.disabled = true
	$Timer.start()

func _on_Timer_timeout():
 get_tree().change_scene("Scenes/MainMenu.tscn")
